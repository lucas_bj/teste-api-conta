package testcases;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import io.restassured.response.ResponseBodyExtractionOptions;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


class TesteApiContaApplicationTests {

  private static final Logger logger = Logger.getLogger(TesteApiContaApplicationTests.class);

  @Parameters({"baseUrl", "conta"})
  @Test
  public void getStatusResponseCode(String baseUrl, String conta) {
    try {
      given()
          .baseUri(baseUrl)
          .basePath(conta)
          .when()
          .get()
          .then()
          .assertThat().statusCode(200);
      logger.debug("\nStatus 200 obtido com sucesso pelo find all.\n");
    } catch (AssertionError ex) {
      logger.debug("\nErro ao retornar status 200 da api pelo find all.\n", ex);
    }
  }

  @Parameters({"baseUrl", "conta"})
  @Test
  public void getInformationsWithSuccess(String baseUrl, String conta) {
    try {
      given().
          baseUri(baseUrl).
          basePath(conta).
          when().
          get().
          then().
          assertThat().body(matchesJsonSchemaInClasspath("GET conta.json"));
      logger.debug("\nJSON Schema validado com sucesso.\n");
    } catch (AssertionError ex) {
      logger.debug("\nJSON Schema inválido.\n", ex);

    }

  }

  @Parameters({"baseUrl", "id"})
  @Test
  public void getAccountIdWithSuccess(String baseUrl, String id) {
    try {
      ResponseBodyExtractionOptions responseBody =
          given().
              baseUri(baseUrl).
              basePath(id).
              when().
              get().
              then().
              extract().body();

      int idConta = responseBody.path("id");

      Assert.assertEquals(idConta, 1);

      logger.debug("\nValor do id da conta validado com sucesso.\n");
    } catch (AssertionError ex) {
      logger.debug("\nValor do id da conta é diferente.\n");
    }
  }

  @Parameters({"baseUrl"})
  @Test
  public void getStatusResponseCode404(String baseUrl) {
    try {
      given()
          .baseUri(baseUrl)
          .basePath("404")
          .when()
          .get()
          .then()
          .assertThat().statusCode(404);
      logger.debug("\nStatus 404 obtido com sucesso pela busca pelo id.\n");
    } catch (AssertionError ex) {
      logger.debug("\nErro ao retornar status 404 da api pela busca pelo id.\n", ex);
    }
  }

  @Parameters({"baseUrl", "id"})
  @Test
  public void getValuesFromAccountWithSuccess(String baseUrl, String id) {
    try {
      ResponseBodyExtractionOptions responseBody =
          given().
              baseUri(baseUrl).
              basePath(id).
              when().
              get().
              then().
              extract().body();

      String agency = responseBody.path("agencia");
      String number = responseBody.path("numero");
      String cpf = responseBody.path("cpf");
      boolean status = responseBody.path("status");

      Assert.assertEquals(agency, "0100");
      Assert.assertEquals(number, "351121");
      Assert.assertEquals(cpf, "02721739085");
      Assert.assertTrue(status);
      logger.debug("\nValores da conta validados com sucesso pela busca pelo id.\n");
    } catch (AssertionError ex) {
      logger.debug("\nValores da conta não conferem pela busca pelo id.\n", ex);
    }
  }

  @Parameters({"baseUrl", "cpf"})
  @Test
  public void getValuesFromAccountThroughCPFWithSuccess(String baseUrl, String cpf) {
    try {
      ResponseBodyExtractionOptions responseBody =
          given().
              baseUri(baseUrl).
              basePath(cpf).
              when().
              get().
              then().
              extract().body();

      String agency = responseBody.path("agencia");
      String number = responseBody.path("numero");
      String accountCpf = responseBody.path("cpf");
      boolean status = responseBody.path("status");

      Assert.assertEquals(agency, "0100");
      Assert.assertEquals(number, "351121");
      Assert.assertEquals(accountCpf, "02721739085");
      Assert.assertTrue(status);
      logger.debug("\nValores da conta validados com sucesso pela busca pelo id.\n");
    } catch (AssertionError ex) {
      logger.debug("\nValores da conta não conferem pela busca pelo id.\n", ex);
    }
  }

  @Parameters({"baseUrl"})
  @Test
  public void getStatusResponseCode404FromFindCpf(String baseUrl) {
    try {
      given()
          .baseUri(baseUrl)
          .basePath("/cpf/404")
          .when()
          .get()
          .then()
          .assertThat().statusCode(404);
      logger.debug("\nStatus 404 obtido com sucesso pela busca de cpf.\n");
    } catch (AssertionError ex) {
      logger.debug("\nErro ao retornar status 404 da api pela busca de cpf.\n", ex);
    }
  }
}
