Nesse projeto é realizada a automação da api <a href="https://www.gitlab.com/lucas_bj/crud_conta">crud_Conta</a> 
<br>O projeto é desenvolvido com Java, Testng e Log4j para registro das evidências dos testes.<br>
Pré-requisitos de execução:<br>
Para que seja possível executar o projeto será necessário instalar a versão do <a href="https://www.oracle.com/technetwork/pt/java/javase/downloads/index.html">Java</a>
 SE 8 ou superior e o <a href="https://maven.apache.org/download.cgi">Maven</a>.
É importante colocar a pasta do maven no mesmo diretório do java, além de acrescentar tanto o java quanto o maven na variável de ambiente PATH (mapear a pasta bin).
<br>Formas de execução:<br>

1ª)Para que os testes passem, é necessário primeiro executar a api crud_Conta, acessar seu swagger e criar uma conta seguindo o exemplo do json no README da api

2ª)Por fim, para executar os testes é necessário acessar o diretório **test.java** e executar o arquivo **testng.xml**.

o teste salva os logs no arquivo **log.out** dentro da pasta log4j.